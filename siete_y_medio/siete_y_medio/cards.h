/* *************************************
Shairah Carpio		October 24, 2016

Interface of a simple Card class
************************************* */

#include <string>
#include <vector>
#include <fstream>

#ifndef CARDS_H
#define CARDS_H

using namespace std;

enum suit_t { OROS, COPAS, ESPADAS, BASTOS };
enum rank_t { AS, DOS, TRES, CUATRO, CINCO, SEIS, SIETE, SOTA = 9, CABALLO = 10, REY = 11 };

class Card {
public:
	Card();
	string get_spanish_suit() const;
	string get_spanish_rank() const;
	string get_english_suit() const;
	string get_english_rank() const;
	int get_rank() const;
	bool operator < (Card card2) const;

private:
	suit_t suit;
	rank_t rank;
};


class Hand {
public:
	Hand();
	Hand(Card card1);
	bool losesRound(Hand dealerHand) const;
	double getTotal() const;
	//void moreCards(string response);
	void addCards(Card cardToAdd);
	void printCards();
	void calculateTotal();
	 
private:
	vector<Card> cards;
	double total;
};


class Player {
public:
	Player(int m);
	int getMoney() const;
	int newTotalMoney(bool losesRound, int bet);
	void read();
private:
	int money;
	int bet;
};

#endif