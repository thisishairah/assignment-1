Copyright: Public domain.
Filename: Siete_y_medio.proj
Purpose: Create a game in which the goal is to obtain cards that add up to a value closest to 7 1/2, but does not encompass this value. 
Assembler: thisishairah
Contact: Shairah Carpio <thisishairah@g.ucla.edu>
Website: https://thisishairah@bitbucket.org/thisishairah/assignment-1.git
Mod history: 2016-10-24

The assignment consists of 3 files: cards.h, cards.cpp and siete_y_medio.cpp. cards.h contains the function declarations for 3 classes: Cards, Hand and Player. cards.cpp contains the definitions for the declarations contained in cards.h. siete_y_medio.cpp runs the program. The program runs until the player no longer has money to bet against the dealer.